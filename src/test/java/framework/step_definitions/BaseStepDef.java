package framework.step_definitions;
import framework.apiCalls.pets.Pets;
import framework.apiCalls.store.Store;
import framework.apiCalls.user.User;
import cucumber.api.java.en.*;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

/**
 * This is the common step definition file for all the feature files.
 * The main reason for creating a single Step definition file is because
 * of the limited number of services. We can create multiple step definition
 * files but it is not recommended for smaller projects
 */

public class BaseStepDef {
    RequestSpecification requestSpecification;
    Response response;
    String fullPath;

    Pets pet = new Pets();
    Store store = new Store();
    User user = new User();

    @Given("Headers accepts content type as {string}")
    public void headers_accepts_content_type_as(String contentType) {
        requestSpecification = given().accept(contentType).contentType(ContentType.JSON);
    }

    @When("User sends GET request to {string}{}")
    public void user_sends_GET_request_to(String path, String id){
        fullPath = path + id;
        response = pet.getPetByID(fullPath);
    }

    @Then("User verifies that response status code is {int}")
    public void user_verifies_that_response_status_code_is(int statusCode) {
        assertEquals(statusCode, response.statusCode());
    }

    @And("User verifies that response content type is {string}")
    public void user_verifies_that_response_content_type_is(String contentType) {
        assertEquals(contentType, response.contentType());
    }

    @When("User sends a GET request to {string}")
    public void user_sends_GET_request_to(String path) {
       response = pet.getPetByStatus(path);
    }

    @And("User selects pets with {string}")
    public void user_selects_pets_with(String string) {
        requestSpecification = given().queryParam("status",string);
    }

    @When("User sends DELETE request to {string}{}")
    public void user_sends_DELETE_request_to(String path, String id) {
        fullPath = path + id;
        response = pet.deletePet(fullPath);
    }

    @And("Select {string} for the authorization filters as a API key")
    public void select_for_the_authorization_filters_as_a_API_key(String key) {
        requestSpecification = given().queryParam("api_key",key);
    }

    @When("User sends POST request to order a pet {string}")
    public void userSendsPOSTRequestToOrderAPet(String path, Map<String,String> orders) {
        response = store.placeOrder(path, orders);
    }

    @And("User verifies the response schema {string}")
    public void userVerifiesTheResponseSchema(String schemaPath) {
        String responseBody = response.getBody().asString();
        assertThat(responseBody,matchesJsonSchemaInClasspath(schemaPath));
    }

    @When("User sends POST request to {string}")
    public void user_sends_POST_request_to(String path, Map<String,String> pets) {
        response = pet.addandUpdatePets(path, pets);
    }

    @When("User sends POST request upload image to {string}")
    public void userSendsPOSTRequestUploadImageTo(String path, Map<String,String> pets) {
        response = pet.uploadImage(path, pets);
    }

    @When("User sends POST request to update pet by id {string}{}")
    public void userSendsPOSTRequestToUpdatePetByIdId(String path, String petID, Map<String,String> pets) {
        String petName = pets.get("petName");
        String petStatus = pets.get("petStatus");

        fullPath = path + petID;
        response = pet.postPetByID(fullPath, petName, petStatus);
    }

    @When("User sends PUT request to {string}")
    public void userSendsPUTRequestTo(String path, Map<String,String> pets) {
        response = pet.addandUpdatePets(path, pets);
    }

    @When("User sends GET request to get order {string}{}")
    public void userSendsGETRequestToGetOrderId(String path, String id){
        fullPath = path + id;
        response = store.getOrderByID(fullPath);
    }

    @When("User sends a GET request to get store inventory {string}")
    public void userSendsAGETRequestToGetStoreInventory(String path) {
        response = store.getOrderByInventory(path);
    }

    @When("User sends DELETE request delete order to {string}{}")
    public void userSendsDELETERequestDeleteOrderToId(String path, String id) {
        fullPath = path + id;
        response = store.deleteOrder(fullPath);
    }

    @When("User sends POST request to create a user {string}")
    public void userSendsPOSTRequestToCreateAUser(String path, Map<String,String> users) {
        response = user.createUser(path, users);
    }

    @And("User verifies that response message contains {string}")
    public void userVerifiesThatResponseMessageContains(String userSession) {
        String message = response.getBody().jsonPath().getString("message");
        Assert.assertTrue(message.contains(userSession));
    }

    @When("User sends GET request to logout {string}")
    public void userSendsGETRequestToLogout(String path) {
        response = user.userLogout(path);
    }

    @When("User sends GET request with {} and {} to {string}")
    public void userSendsGETRequestWithUsernameAndPasswordTo(String username, String password, String path) {
        response = user.userLogin(path, username, password);
    }

    @When("User sends GET request to get user by {} {string}")
    public void userSendsGETRequestToGetUserByUsername(String username, String path) {
        fullPath = path + username;
        response = user.getUserByUserName(fullPath);
    }

    @When("User sends PUT request to update user by {} {string}")
    public void userSendsPUTRequestToUpdateUserByUsername(String username, String path) {
        fullPath = path + username;
        response = user.updateUser(fullPath);
    }

    @When("User sends DELETE request to delete user by {} {string}")
    public void userSendsDELETERequestToDeleteUserByUsername(String username, String path) {
        fullPath = path + username;
        response = user.deleteUser(fullPath);
    }

}