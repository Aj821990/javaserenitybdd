package framework.apiCalls.user;

import framework.apiCalls.BaseCalls;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;

/**
 * All service calls to user service is covered in this class.
 * All the below methods returns response for the particular rest calls
 * which is then handled at step definition file for validation
 */

public class User extends BaseCalls {
    Response response = null;
    JSONObject body = new JSONObject();

    /**
     * This is the POST service call to create user.
     * This method handles three service calls i.e. create single user,
     * create multiple users with array and create multiple users with list
     * @param path
     * @param users
     * @return
     */
    public Response createUser(String path, Map<String,String> users) {

        body.clear();
        body.put("id", users.get("userID"));
        body.put("username", users.get("username"));
        body.put("firstName", users.get("firstName"));
        body.put("lastName", users.get("lastName"));
        body.put("email", users.get("email"));
        body.put("password", users.get("password"));
        body.put("phone", users.get("phone"));
        body.put("userStatus", users.get("userStatus"));

        switch (path) {
            case "user":
                response = given()
                        .spec(SetBaseUri())
                        .header("accept", "application/json")
                        .header("Content-Type", "application/json")
                        .body(body)
                        .post(path);
                break;

            case "user/createWithArray":

            case "user/createWithList":
                response = given()
                        .spec(SetBaseUri())
                        .header("accept", "application/json")
                        .header("Content-Type", "application/json")
                        .body("["+body+"]")
                        .post(path);
                break;
        }

        return response;
    }

    /**
     * This is the GET service call to login a user
     * @param path
     * @param userName
     * @param passWord
     * @return
     */
    public Response userLogin(String path, String userName, String passWord) {
        RequestSpecification request = RestAssured.given();

        response = request
                .spec(SetBaseUri())
                .header("accept", "application/json")
                .queryParam("username", userName)
                .queryParam("password", passWord)
                .get(path);

        return response;
    }

    /**
     * This is the GET service call to logout a user
     * @param path
     * @return
     */
    public Response userLogout(String path) {
        response = given()
                .spec(SetBaseUri())
                .header("accept", "application/json")
                .get(path);

        return response;
    }

    /**
     * This is the GET service call to get a user by username
     * @param path
     * @return
     */
    public Response getUserByUserName(String path) {
        response = given()
                .spec(SetBaseUri())
                .header("accept", "application/json")
                .get(path);
        return response;
    }

    /**
     * This is the PUT service call to update a user
     * @param path
     * @return
     */
    public Response updateUser(String path) {
        response = given()
                .spec(SetBaseUri())
                .header("accept", "application/json")
                .header("Content-Type", "application/json")
                .body("{}")
                .put(path);

        return response;
    }

    /**
     * This is the DELETE service call to delete a user
     * @param path
     * @return
     */
    public Response deleteUser(String path) {
        response = given()
                .spec(SetBaseUri())
                .header("accept", "application/json")
                .delete(path);

        return response;
    }
}
