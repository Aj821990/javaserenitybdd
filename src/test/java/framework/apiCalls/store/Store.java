package framework.apiCalls.store;

import framework.apiCalls.BaseCalls;
import io.restassured.response.Response;
import org.json.simple.JSONObject;

import java.util.Map;

import static io.restassured.RestAssured.given;

/**
 * All service calls to store service is covered in this class.
 * All the below methods returns response for the particular rest calls
 * which is then handled at step definition file for validation
 */

public class Store extends BaseCalls {
    Response response = null;
    JSONObject body = new JSONObject();

    /**
     * This is the POST service call to place a new order
     * @param path
     * @param orders
     * @return
     */
    public Response placeOrder(String path, Map<String,String> orders) {

        body.clear();
        body.put("id", orders.get("orderID"));
        body.put("petId", orders.get("petId"));
        body.put("quantity", orders.get("quantity"));
        body.put("shipDate", orders.get("shipDate"));
        body.put("status", orders.get("orderStatus"));
        body.put("complete", orders.get("orderComplete"));

        response = given()
                .spec(SetBaseUri())
                .header("accept", "application/json")
                .header("Content-Type", "application/json")
                .body(body)
                .post(path);

        return response;
    }

    /**
     * This is the GET service call to get order by order id
     * @param path
     * @return
     */
    public Response getOrderByID(String path) {
        response = given()
                .spec(SetBaseUri())
                .header("accept", "application/json")
                .get(path);

        return response;
    }

    /**
     * This is the GET service call to get order by inventory
     * @param path
     * @return
     */
    public Response getOrderByInventory(String path) {
        response = given()
                .spec(SetBaseUri())
                .header("accept", "application/json")
                .get(path);
        return response;
    }

    /**
     * This is the DELETE service call to delete an order
     * @param path
     * @return
     */
    public Response deleteOrder(String path) {
        response = given()
                .spec(SetBaseUri())
                .header("accept", "application/json")
                .delete(path);
        return response;
    }
}