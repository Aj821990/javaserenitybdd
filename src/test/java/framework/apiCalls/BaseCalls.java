package framework.apiCalls;

import framework.utils.ConfigurationReader;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

/**
 * This is the base class where common methods are declared
 */

public class BaseCalls {

    public static RequestSpecification SetBaseUri()
    {
        RequestSpecification requestSpec = new RequestSpecBuilder().build();
        requestSpec.baseUri(ConfigurationReader.get("baseURI"));

        return requestSpec;
    }
}
