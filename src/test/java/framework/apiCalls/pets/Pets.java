package framework.apiCalls.pets;

import framework.apiCalls.BaseCalls;
import io.restassured.response.Response;
import org.json.simple.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;

/**
 * All service calls to pet service is covered in this class.
 * All the below methods returns response for the particular rest calls
 * which is then handled at step definition file for validation
 */

public class Pets extends BaseCalls {
    Response response = null;
    JSONObject body = new JSONObject();

    /**
     * This is the service call to add and update the pets.
     * As all the inputs are same for post and put method
     * only single method is created to handle it.
     * @param path
     * @param pets
     * @return
     */
    public Response addandUpdatePets(String path, Map<String,String> pets) {
        List<String> urlPhoto = new ArrayList<>();
        urlPhoto.add(pets.get("photoUrls"));

        List<Map<String, Object>> tagsList = new ArrayList<>();

        Map<String, Object> tags = new HashMap<>();
        tags.put("id", pets.get("tagsID"));
        tags.put("name", pets.get("tagsName"));

        tagsList.add(tags);

        Map<String, Object> category = new HashMap<>();
        category.put("id", pets.get("categoryID"));
        category.put("name", pets.get("categoryName"));

        body.clear();
        body.put("id", pets.get("petID"));
        body.put("category", category);
        body.put("name", pets.get("petName"));
        body.put("photoUrls", urlPhoto);
        body.put("tags", tagsList);
        body.put("status", pets.get("petStatus"));

        response = given()
                .spec(SetBaseUri())
                .header("accept", "application/json")
                .header("Content-Type", "application/json")
                .body(body)
                .post(path);

        return response;
    }

    /**
     * This is a POST service call to upload an image for pet
     * @param path
     * @param pets
     * @return
     */
    public Response uploadImage(String path, Map<String,String> pets)
    {
        String fullPath = "pet/" + pets.get("petID") + path;
        File imageFile = new File(System.getProperty("user.dir")+"//src//test//resources//images//" + pets.get("fileToUpload"));
        response = given()
                .spec(SetBaseUri())
                .header("accept", "application/json")
                .contentType("multipart/form-data")
                .formParam("additionalMetadata", pets.get("additionalMetadata"))
                .multiPart("file", imageFile, "image/jpeg")
                .post(fullPath);

        System.out.println("response is = " + response.getBody().asString());

        return response;
    }

    /**
     * This is the GET service call to get pet informations by pet id
     * @param path
     * @return
     */
    public Response getPetByID(String path) {
        response = given()
                .spec(SetBaseUri())
                .header("accept", "application/json")
                .get(path);

        return response;
    }

    /**
     * This is the  GET service call to get pet informations by status
     * @param path
     * @return
     */
    public Response getPetByStatus(String path) {
        response = given()
                .spec(SetBaseUri())
                .header("accept", "application/json")
                .get(path);
        return response;
    }

    /**
     * This is the DELETE service call to delete a pet
     * @param path
     * @return
     */
    public Response deletePet(String path) {
        response = given()
                .spec(SetBaseUri())
                .header("accept", "application/json")
                .delete(path);
        return response;
    }

    /**
     * This is the POST service call to update pet by pet id
     * @param path
     * @param petName
     * @param petStatus
     * @return
     */
    public Response postPetByID(String path, String petName, String petStatus)
    {
        response = given()
                .spec(SetBaseUri())
                .header("accept", "application/json")
                .header("Content-Type", "application/x-www-form-urlencoded")
                .body("name="+petName+"&status="+petStatus)
                .post(path);
        return response;
    }
}
