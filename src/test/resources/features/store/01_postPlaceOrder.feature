@SmokeTest
Feature: Place order API Test

  Background:
    Given Headers accepts content type as "application/json"

  Scenario Outline: User should be able to place orders for a pet
    When User sends POST request to order a pet "store/order"
      | orderID       | <orderID>       |
      | petId         | <petId>         |
      | quantity      | <quantity>      |
      | shipDate      | <shipDate>      |
      | orderStatus   | <orderStatus>   |
      | orderComplete | <orderComplete> |
    Then User verifies that response status code is <statusCode>
    And User verifies that response content type is "application/json"
    And User verifies the response schema "schema/postPlaceOrder.json"

    Examples:
      | orderID | petId    | quantity | shipDate                 | orderStatus | orderComplete | statusCode |
      #passing all valid values and orderStatus = placed
      | 11      | 111      | 1        | 2020-09-03T08:59:34.021Z | placed      | true          | 200        |
      #passing all valid values and orderStatus = approved
      | 22      | 111      | 2        | 2022-09-03T08:59:34.021Z | approved    | true          | 200        |
      #passing all valid values and orderStatus = delivered
      | 33      | 111      | 1        | 2020-09-03T08:59:34.021Z | delivered   | true          | 200        |
      #passing all invalid petID
      | 44      | 49876533 | 1        | 2020-09-03T08:59:34.021Z | delivered   | true          | 400        |
      #passing all invalid quantity
      | 55      | 111      | -1       | 2020-09-03T08:59:34.021Z | delivered   | true          | 400        |
      #passing all invalid/past shipDate
      | 66      | 111      | 1        | 2010-09-03T08:59:34.021Z | delivered   | true          | 400        |
      #passing all invalid orderStatus
      | 77      | 111      | 1        | 2020-09-03T08:59:34.021Z | anything    | true          | 400        |
      #passing all invalid orderComplete
      | 88      | 111      | 1        | 2020-09-03T08:59:34.021Z | delivered   | anything      | 400        |
