@SmokeTest
Feature: Get order by id API Test

  Background:
    Given Headers accepts content type as "application/json"

  Scenario Outline: User should be able to get order by id for a pet
    When User sends GET request to get order "store/order/"<id>
    Then User verifies that response status code is <statusCode>
    And User verifies that response content type is "<contentType>"
    And User verifies the response schema "schema/getOrderByID.json"

    Examples:
      | id    | statusCode | contentType      |
      | 1     | 200        | application/json |
      | 44454 | 404        | application/json |
      | abcd  | 400        | application/json |
      |       | 400        | application/json |