@SmokeTest
Feature: Get order by inventory API Test

  Background:
    Given Headers accepts content type as "application/json"

  Scenario Outline: User should be able to get orders by inventory
    When User sends a GET request to get store inventory "store/inventory"
    Then User verifies that response status code is <statusCode>
    And User verifies that response content type is "application/json"
    And User verifies the response schema "schema/getStoreInventory.json"

    Examples:
      | statusCode |
      | 200        |
