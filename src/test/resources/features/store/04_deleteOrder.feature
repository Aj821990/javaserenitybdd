@SmokeTest
Feature: Delete order API Test

  Background:
    Given Headers accepts content type as "application/json"

  Scenario Outline: User should be able to delete an existing order with order id
    When User sends DELETE request delete order to "store/order/"<id>
    Then User verifies that response status code is <statusCode>
    #And User verifies the response schema "schema/deleteOrder.json"

    Examples:
      | id     | statusCode |
      | 1      | 200        |
      | 370012 | 404        |
      | abcd   | 400        |
      |        | 400        |
