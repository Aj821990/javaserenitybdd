@SmokeTest
Feature: Get pet by status API Test

  Background:
    Given Headers accepts content type as "application/json"

  Scenario Outline: User should be able to find all pets by status
    When User sends a GET request to "store/inventory"
    And User selects pets with "<status>"
    Then User verifies that response status code is <statusCode>
    And User verifies that response content type is "application/json"
    And User verifies the response schema "schema/getPetByStatus.json"

    Examples:
      | status    | statusCode |
      | available | 200        |
      | pending   | 200        |
      | sold      | 200        |
      | anything  | 400        |
      |           | 400        |
