@SmokeTest
Feature: Post add pet API Test

  Background:
    Given Headers accepts content type as "application/json"

  Scenario Outline: User should be able to create a pet
    When User sends POST request to "/pet"
      | petID        | <petID>        |
      | categoryID   | <categoryID>   |
      | categoryName | <categoryName> |
      | petName      | <petName>      |
      | photoUrls    | <photoUrls>    |
      | tagsID       | <tagsID>       |
      | tagsName     | <tagsName>     |
      | petStatus    | <petStatus>    |

    Then User verifies that response status code is <statusCode>
    And User verifies that response status code is <statusCode>
    And User verifies that response content type is "application/json"
    And User verifies the response schema "schema/postAddPet.json"

    Examples:
      | petID | categoryID | categoryName  | petName | photoUrls | tagsID | tagsName | petStatus | statusCode |
      #passing all fields and pet status = available
      | 111   | 1111       | categoryName1 | name1   | url1      | 11111  | tagName1 | available | 200        |
      #passing all fields and pet status = pending
      | 222   | 2222       | categoryName2 | name2   | url2      | 22222  | tagName2 | pending   | 200        |
      #passing all fields and pet status = sold
      | 333   | 3222       | categoryName3 | name3   | url3      | 33333  | tagName1 | sold      | 200        |
      #passing all fields except petID
      |       | 3222       | categoryName3 | name3   | url3      | 33333  | tagName1 | sold      | 200        |
      #passing all fields except categoryID
      | 333   |            | categoryName3 | name3   | url3      | 33333  | tagName1 | sold      | 200        |
      #passing all fields except vategoryName
      | 333   | 3222       |               | name3   | url3      | 33333  | tagName1 | sold      | 405        |
      #passing all fields except petName
      | 333   | 3222       | categoryName3 |         | url3      | 33333  | tagName1 | sold      | 405        |
      #passing all fields except photourl
      | 333   | 3222       | categoryName3 | name3   |           | 33333  | tagName1 | sold      | 405        |
      #passing all fields except tagid
      | 333   | 3222       | categoryName3 | name3   | url3      |        | tagName1 | sold      | 200        |
      #passing all fields except tagName
      | 333   | 3222       | categoryName3 | name3   | url3      | 33333  |          | sold      | 405        |
      #passing all fields except status
      | 333   | 3222       | categoryName3 | name3   | url3      | 33333  | tagName1 |           | 405        |
