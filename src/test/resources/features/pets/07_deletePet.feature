@SmokeTest
Feature: Delete pet API Test

  Background:
    Given Headers accepts content type as "application/json"

  Scenario Outline: User should be able to delete an existing pet with API key and pet id
    When User sends DELETE request to "pet/"<id>
    And Select "special-key" for the authorization filters as a API key
    Then User verifies that response status code is <statusCode>
    #And User verifies the response schema "schema/deletePet.json"

    Examples:
      | id     | statusCode |
      | 333    | 200        |
      | 370012 | 404        |
      | abcd   | 400        |
      |        | 400        |
