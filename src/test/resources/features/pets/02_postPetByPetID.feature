@SmokeTest
Feature: Post updates pet by pet id API Test

  Background:
    Given Headers accepts content type as "application/json"

  Scenario Outline: User should be able to update pet information by id
    When User sends POST request to update pet by id "pet/"<petID>
      | petName   | <petName>   |
      | petStatus | <petStatus> |
    Then User verifies that response status code is <statusCode>
    And User verifies that response content type is "<contentType>"
    And User verifies the response schema "schema/postPetByID.json"

    Examples:
      | petID | petName | petStatus | statusCode | contentType      |
      | 111   | name10  | sold      | 200        | application/json |
      | 44454 | name10  | sold      | 404        | application/json |
      | abcd  | name10  | sold      | 405        | application/json |
      |       | name10  | sold      | 405        | application/json |
