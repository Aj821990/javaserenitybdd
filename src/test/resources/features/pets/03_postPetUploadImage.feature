@SmokeTest
Feature: Post add pet by upload image API Test

  Background:
    Given Headers accepts content type as "application/json"

  Scenario Outline: User should be able to create a pet
    When User sends POST request upload image to "/uploadImage"
      | petID              | <petID>              |
      | additionalMetadata | <additionalMetadata> |
      | fileToUpload       | <fileToUpload>       |

    Then User verifies that response status code is <statusCode>
    And User verifies that response status code is <statusCode>
    And User verifies that response content type is "application/json"
    And User verifies the response schema "schema/postAddPet.json"

    Examples:
      | petID | additionalMetadata | fileToUpload | statusCode |
      | 111   | doggy1             | puppy1.jpg   | 200        |
      | 87564 | doggy1             | puppy1.jpg   | 404        |
      |       | doggy1             | puppy1.jpg   | 400        |
      | 111   |                    | puppy1.jpg   | 400        |