@SmokeTest
Feature: Get pet by id API Test

  Background:
    Given Headers accepts content type as "application/json"

  Scenario Outline: User should be able to get pet information by id
    When User sends GET request to "pet/"<id>
    Then User verifies that response status code is <statusCode>
    And User verifies that response content type is "<contentType>"

    Examples:
      | id    | statusCode | contentType      |
      | 111   | 200        | application/json |
      | 44454 | 404        | application/json |
      | abcd  | 400        | application/json |
      |       | 400        | application/json |
