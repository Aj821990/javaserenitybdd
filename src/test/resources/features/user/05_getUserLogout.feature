@SmokeTest
Feature: Get user logout API Test

  Background:
    Given Headers accepts content type as "application/json"

  Scenario Outline: User should be able to logout
    When User sends GET request to logout "user/logout"
    Then User verifies that response status code is <statusCode>
    And User verifies that response message contains "ok"
    And User verifies that response content type is "application/json"

    Examples:
      | statusCode |
      | 200        |
