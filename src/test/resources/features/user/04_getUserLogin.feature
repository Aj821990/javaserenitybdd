@SmokeTest
Feature: Get user login API Test

  Background:
    Given Headers accepts content type as "application/json"

  Scenario Outline: User should be able to login
    When User sends GET request with <username> and <password> to "user/login"
    Then User verifies that response status code is <statusCode>
    And User verifies that response message contains "logged in user session:"
    And User verifies that response content type is "application/json"

    Examples:
      | username                       | password            | statusCode |
      | username1                      | username1           | 200        |
      | qwdq4tqrt245g25345463575646274 | qk;iohvaqv/kvwh;vxv | 400        |
      |                                |                     | 400        |
