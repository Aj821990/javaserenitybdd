@SmokeTest
Feature: Update user API Test

  Background:
    Given Headers accepts content type as "application/json"

  Scenario Outline: User should be able to update user
    When User sends PUT request to update user by <username> "user/"
    Then User verifies that response status code is <statusCode>
    And User verifies that response content type is "application/json"

    Examples:
      | username         | statusCode |
      | abc              | 200        |
      | username1        | 404        |
      | /////!@#$%^&*(() | 400        |
      |                  | 400        |
