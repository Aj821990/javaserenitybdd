@SmokeTest
Feature: Get user by username API Test

  Background:
    Given Headers accepts content type as "application/json"

  Scenario Outline: User should be able to get user by username
    When User sends GET request to get user by <username> "user/"
    Then User verifies that response status code is <statusCode>
    And User verifies that response content type is "application/json"

    Examples:
      | username         | statusCode |
      | abc              | 200        |
      | username1        | 404        |
      | /////!@#$%^&*(() | 400        |
      |                  | 400        |
