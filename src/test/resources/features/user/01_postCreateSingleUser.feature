@SmokeTest
Feature: Create single user API Test

  Background:
    Given Headers accepts content type as "application/json"

  Scenario Outline: User should be able to create user
    When User sends POST request to create a user "user"
      | userID     | <userID>     |
      | username   | <username>   |
      | firstName  | <firstName>  |
      | lastName   | <lastName>   |
      | email      | <email>      |
      | password   | <password>   |
      | phone      | <phone>      |
      | userStatus | <userStatus> |

    Then User verifies that response status code is <statusCode>
    And User verifies that response content type is "application/json"

    Examples:
      | userID      | username    | firstName   | lastName    | email       | password  | phone       | userStatus  | statusCode |
      #passing all valid parameters
      | 1           | username1   | firstname1  | lastname1   | username1   | username1 | 1111111111  | 1           | 200        |
      #passing all invalid userID
      | 123!@$^&)(& | username2   | firstname2  | lastname2   | username2   | username2 | 2222222222  | 2           | 400        |
      #passing all invalid userName
      | 3           | 123!@$^&)(& | firstname3  | lastname3   | username3   | username3 | 3333333333  | 3           | 400        |
      #passing all invalid firstName
      | 4           | username4   | 123!@$^&)(& | lastname4   | username4   | username4 | 4444444444  | 4           | 400        |
      #passing all invalid lastName
      | 5           | username5   | firstname5  | 123!@$^&)(& | username5   | username5 | 5555555555  | 5           | 400        |
      #passing all invalid email
      | 6           | username6   | firstname6  | lastname6   | 123!@$^&)(& | username6 | 6666666666  | 6           | 400        |
      #passing all blank password
      | 7           | username6   | firstname6  | lastname6   | username6   |           | 6666666666  | 6           | 400        |
      #passing all invalid phone
      | 8           | username6   | firstname6  | lastname6   | username6   | username6 | 123!@$^&)(& | 6           | 400        |
      #passing all invalid userStatus
      | 9           | username6   | firstname6  | lastname6   | username6   | username6 | 6666666666  | 123!@$^&)(& | 400        |
