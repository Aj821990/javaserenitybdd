# JavaSerenityBDD

API Automation with Selenium Rest-Assured, Java8, Maven, Serenity BDD Framework and gitlab-ci

## Requirements:
Below dependencies needs to be installed/configured
- Java 8 or higher (JAVA_HOME and PATH in environmental variables)
- Maven (M2, MAVEN_HOME and PATH in environmental variables)
- IDE (IntelliJ is preferred)
- It would be nice to have "Cucumber for Java" and "Gherkin" plugins installed in your IDE for better understanding and working of cucumber

## Downloading Project:
- Using git command and clone with HTTPS:
git clone https://gitlab.com/Aj821990/javaserenitybdd.git

*or*

- Using git command and SSH key is configured in your system:
git clone https://gitlab.com/Aj821990/javaserenitybdd.git

*or*

- Download the project from https://gitlab.com/Aj821990/javaserenitybdd.git

## Creating new feature file
- Existing feature files are currently located at "src/test/resources/features" with separate folders for each service
- If you want to edit existing feature file or add new feature file for the same service, you can start creating a feature file in the specified folder structures
- If it is a new service, you can create a new folder with that service name and start creating feature files
- In case you do not want to pass any data values in feature file, please create a feature file with "Scenario" instead of "Scenario Outline" and remove the "Examples" section
- All the steps you write should be in Gherkin language (for reference see existing feature files)
- When you start typing your steps, you will get options to reuse the steps. You can choose to select the existing step or create a new step based on your scenario to be tested

## Creating new step definition
- Once you create a (new) step in feature file, you will see your steps being highlighted as you have not created any step definition for your steps in feature file
- You can select where you want your step definition to be written.
- In this project, as there is only 1 step definition file at location "src/test/java/framework/step_definitions", you can either choose to create your step definition there or create a new file.
- In-case you choose to create a new step definition file, make sure you create it in the same package as that of the existing step definition
- Once you created your default step defintion, put the logic you want the step definition to do

## Execution:

In-case of errors, please check/update your dependencies in pom.xml file based on your IDE and System configurations.

1.. Run via runner file
````sh
Right click on the runner file and select Run
Example:
Runner file in this project is CukesRunnerTest. Right click and select -> Run 'CukesRunnerTest'
````
2.. Run via terminal
```sh
mvn verify
```
3.. Run via gitlab CI pipeline
```sh
All commands are configure in .gitlab-ci.yml file. So you can directly run the project via gitlab ci pipeline
```

## Important points to remember:
- ***Scenarios covered:*** Scenarios only with the header "Content-Type: application/json" is covered
- ***Reporting:*** Serenity BDD framework default reporting is used in this project
- ***Test Report:*** Test Report is saved in location - "target/site/serenity/index.html"
                     Test report will be visible locally only after executing the project from your terminal with command "mvn verify"
- ***.gitignore:*** .idea/, target/ and .iml are covered in .gitignore file
- ***Step Definitions:*** There is only single step definition file configured in this project as the services to be covered are less.
                          We can split the step definitions in three/four different files as per services (example: pets, store and user)
- ***CI pipeline:*** The gitlab ci pipeline will fail as the scenarios are failing.
- ***Artifacts:*** Artifacts after executing the pipeline can be downloaded from gitlab. The HTML report will not be in great shape as we are just sharing the final HTML report and not the entire package.
- ***Service Interactions:*** End to end flow is not covered yet. This project currently verifies each service one at a time.
- ***Coverage:*** All end points are covered but not all scenarios are covered.

Please get in touch with me at *antojohn.8@gmail.com* for any queries regarding this project